#!/bin/sh
set -e

#sudo apt-get install libmysqlclient-dev python3-dev
#pyenv install $(cat .python-version)
#pyenv local $(cat .python-version)
#pip install -r requirements.txt


(docker ps -f name=mysqlc | grep mysqlc > /dev/null )  && docker stop mysqlc

sleep 3

docker run -d --name=mysqlc -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_USERNAME=root -e MYSQL_ROOT_PASSWORD=root -v "$(pwd)/data:/var/lib/mysql" -p 3316:3306 --rm mysql:5.7 mysqld &

while ! mysqladmin ping -h "127.0.0.1" -P 3316 ; do
  sleep 1
done

python ./data_import.py
echo 'end of the import'
python ./bot.py

